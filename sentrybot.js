
var port=process.env.PORT;

var express = require('express');
var app = express.createServer();

var nodeNxt = require('node-nxt');
var usbPort = '/dev/ttyACM0';
var ltEngPort = 2;
var rtEngPort = 3;

app.configure(function(){
    app.use(express.static(__dirname + '/static'));
//    app.use(express.methodOverride());
//    app.use(express.bodyParser());
//    app.use(app.router);
});

app.all('/',function(req,res,next) {
//    next();
    res.sendfile(__dirname +'/static/dashboard.html');
});

app.get('/nxt/:left/:right', function(req, res){
  console.log('Left: '+req.params.left+' Right: '+req.params.right);
  
  nodeNxt.connect (usbPort, function (nxt) {
    nxt.OutputSetSpeed(ltEngPort, 32, req.params.left );
    nxt.OutputSetSpeed(rtEngPort, 32, req.params.right );
  });
  
  
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end('nxt control done\n');
});

app.listen(port);